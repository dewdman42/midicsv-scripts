# midicsv-scripts

Collection of **midicsv** based scripts for manipulating midi files.  These require **midicsv** utility to be installed in order to work.  They will usually incluced calls to **midicsv** to convert a midifile to `CSV` format and then convert back to midifile from that.


| Script | Description |
| ------ | ------ |
| **tempocleaner.awk** | `AWK` script that can filter out redundant Tempo and Time Sig entries when there are more than one on the same timestamp, keeps only the last one. |
| **cuepoints.awk** | Convert Cuepoint markers into simple text markers |



#!/bin/sh

#================================================
# Convert Cue Point markers into Text markers
#================================================

midicsv $1 |\
awk 'BEGIN {FS=", "; OFS=", "}
$3 == "Cue_point_t" {
    $3 = "Marker_t"
    print
    next
}
{ print }' | csvmidi 

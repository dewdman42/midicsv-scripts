#!/bin/sh

#================================================
# filter redundant tempo and time sig changes
# when they are on the same timestamp.  Keep last
#================================================

midicsv $1 |\
awk 'BEGIN {FS=", "; OFS=", "}
{
   if( $2 != lastTime || $1 != lastTrack) {
       lastTime = $2
       lastTrack = $1

       if(last["Tempo"] != NULL) {
           print last["Tempo"]
           last["Tempo"] = NULL
       }
       if(last["Time_signature"] != NULL) {
           print last["Time_signature"]
           last["Time_signature"] = NULL
       }
       if($3 == "Tempo" || $3 == "Time_signature") {
           last[$3] = $0
           next
       }
       else {
           print
       }
   }

   else {
        if($3 == "Tempo" || $3 == "Time_signature") {
           last[$3] = $0
           next
       }
       else {
           print
       }
   }
}' | csvmidi 
